package com.users.demo.utils;

import com.users.demo.domain.Role;
import com.users.demo.domain.Status;
import com.users.demo.domain.User;
import com.users.demo.dto.UserDto;

public class TestUtils {
    public static final User MYUSER0;
    public static final UserDto MYUSER0_DTO;

    static {
        MYUSER0 = new User(1L, "myname0", "1234560", "myname0@gmail.com",
                "+79217548783", Status.ONLINE, false, Role.USER);
        MYUSER0_DTO = new UserDto(MYUSER0.getId(), MYUSER0.getName(), MYUSER0.getPassword(), MYUSER0.getEmail(),
                MYUSER0.getPhoneNumber(), MYUSER0.getStatus(), MYUSER0.getRole());
    }
}
