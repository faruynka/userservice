package com.users.demo.service;


import com.users.demo.repository.UserRepo;
import com.users.demo.service.impl.UserServiceImpl;
import com.users.demo.utils.TestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {UserServiceImpl.class})
class UserServiceImplTest {
    @Autowired
    private UserService userService;

    @MockBean
    private UserRepo userRepo;

    @Test
    void findSelfTest() {
        Mockito.when(userRepo.findById(TestUtils.MYUSER0.getId()))
                .thenReturn(Optional.of(TestUtils.MYUSER0));
        assertEquals(TestUtils.MYUSER0, userService.find(TestUtils.MYUSER0.getId()));
    }

    @Test
    void findSelfEntityNotFoundTest() {
        Mockito.when(userRepo.findById(TestUtils.MYUSER0.getId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.find(TestUtils.MYUSER0.getId()));
    }

    @Test
    void addTest() {
        Mockito.when(userRepo.findByName(TestUtils.MYUSER0.getName())).
                thenReturn(Optional.empty());
        Mockito.when(userRepo.save(TestUtils.MYUSER0)).
                thenReturn(TestUtils.MYUSER0);
        assertEquals(TestUtils.MYUSER0, userService.add(TestUtils.MYUSER0));
    }

    @Test
    void addUserExistTest() {
        Mockito.when(userRepo.findByName(TestUtils.MYUSER0.getName())).
                thenReturn(Optional.of(TestUtils.MYUSER0));
        Assertions.assertThrows(EntityExistsException.class,
                () -> userService.add(TestUtils.MYUSER0));
    }
}