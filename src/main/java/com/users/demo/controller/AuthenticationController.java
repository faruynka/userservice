package com.users.demo.controller;

import com.users.demo.domain.User;
import com.users.demo.dto.LoginDto;
import com.users.demo.dto.UserDto;
import com.users.demo.service.UserService;
import com.users.demo.transformer.Transformer;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/login")
public class AuthenticationController {
    private final UserService userService;
    private final Transformer<User, UserDto> userTransformer;

    public AuthenticationController(UserService userService, Transformer<User, UserDto> userTransformer) {
        this.userService = userService;
        this.userTransformer = userTransformer;
    }

    @GetMapping
    public String login() {
        return "Hello user. Log in, please.";
    }

    @PostMapping
    public UserDto login(@Valid @RequestBody LoginDto loginDto) {
        return userTransformer.parseToDto(userService.login(loginDto.getName(), loginDto.getPassword()));
    }
}
