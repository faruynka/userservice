package com.users.demo.controller;

import com.users.demo.domain.Status;
import com.users.demo.domain.User;
import com.users.demo.dto.UserDto;
import com.users.demo.dto.UserStatusDto;
import com.users.demo.service.UserService;
import com.users.demo.transformer.Transformer;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;
    private final Transformer<User, UserDto> userTransformer;

    public UserController(UserService userService, Transformer<User, UserDto> userTransformer) {
        this.userService = userService;
        this.userTransformer = userTransformer;
    }

    @PostMapping("/registration")
    public Long add(@Valid @RequestBody UserDto userDto) {
        User user = userService.add(userTransformer.parseToEntity(userDto));
        return user.getId();
    }

    @GetMapping("/info")
    public UserDto getInfo(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId) {
        return userTransformer.parseToDto(userService.find(userId));
    }


    @PostMapping("/update/status")
    public UserStatusDto changeStatus(@Valid @RequestBody UserStatusDto userStatusDto) throws Exception {
        Status lastStatus = userService.changeStatus(userStatusDto.getId(), userStatusDto.getStatus());
        userStatusDto.setLastStatus(lastStatus);
        return userStatusDto;
    }
}
