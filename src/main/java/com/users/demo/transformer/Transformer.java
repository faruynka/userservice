package com.users.demo.transformer;

import java.util.List;
import java.util.stream.Collectors;

public interface Transformer<T, G> {

    default T parseToEntity(G dto) {
        return parseToEntity(null, dto);
    }

    T parseToEntity(T entity, G dto);

    G parseToDto(T entity);

    default List<G> parseToDto(List<T> entities) {
        return entities.stream().map(this::parseToDto).collect(Collectors.toList());
    }
}
