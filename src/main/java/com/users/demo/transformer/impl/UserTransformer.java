package com.users.demo.transformer.impl;

import com.users.demo.domain.User;
import com.users.demo.dto.UserDto;
import com.users.demo.transformer.Transformer;
import org.springframework.stereotype.Component;

@Component
public class UserTransformer implements Transformer<User, UserDto> {
    @Override
    public User parseToEntity(User user, UserDto userDto) {
        if (userDto == null) return null;
        if (user == null) user = new User();
        if (userDto.getId() != null) user.setId(userDto.getId());
        if (userDto.getName() != null) user.setName(userDto.getName());
        if (userDto.getPassword() != null) user.setPassword(userDto.getPassword());
        if (userDto.getEmail() != null) user.setEmail(userDto.getEmail());
        if (userDto.getPhoneNumber() != null) user.setPhoneNumber(userDto.getPhoneNumber());
        if (userDto.getRole() != null) {
            user.setRole(userDto.getRole());
        }
        return user;
    }

    @Override
    public UserDto parseToDto(User user) {
        if (user == null) {
            return null;
        }
        UserDto userDto = new UserDto();
        if (user.getId() != null) {
            userDto.setId(user.getId());
        }
        if (user.getPassword() != null) {
            userDto.setPassword(user.getPassword());
        }
        if (user.getName() != null) {
            userDto.setName(user.getName());
        }
        if (user.getEmail() != null) {
            userDto.setEmail(user.getEmail());
        }
        if (user.getPhoneNumber() != null) {
            userDto.setPhoneNumber(user.getPhoneNumber());
        }
        if (user.getStatus() != null) {
            userDto.setStatus(user.getStatus());
        }
        if (user.getRole() != null) {
            userDto.setRole(user.getRole());
        }
        return userDto;
    }
}
