package com.users.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class LoginDto {
    @NotBlank(message = "User name is required")
    @Size(min = 2, max = 30, message = "Name size should be more than 2 symbols and less than 30 sumbols")
    private String name;

    @NotBlank
    private String password;

    public LoginDto() {
    }
}
