package com.users.demo.dto;

import com.users.demo.domain.Status;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class UserStatusDto {
    @Id
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    @Enumerated(EnumType.STRING)
    private Status lastStatus;

    public UserStatusDto() {
    }
}
