package com.users.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import com.users.demo.domain.Role;
import com.users.demo.domain.Status;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class UserDto {

    private Long id;

    @NotBlank(message = "User name is required")
    @Size(min = 2, max = 30, message = "Name size should be more than 2 symbols and less than 30 sumbols")
    private String name;

    @NotBlank
    private String password;

    @Email(message = "Email is incorrect")
    private String email;

    @Pattern(regexp = "\\+7[0-9]{10}")
    private String phoneNumber;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Enumerated(EnumType.STRING)
    private Role role;

    public UserDto() {
    }

    public void setRole(Role role) {
        if (role == null) {
            role = Role.USER;
        }
        this.role = role;
    }
}
