package com.users.demo.job;

import com.users.demo.service.UserService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class DeleteTimerJob {
    private final UserService userService;

    public DeleteTimerJob(UserService userService) {
        this.userService = userService;
    }

    @Scheduled(cron = "* */10 * * * *")
    public void deleteUsers() {
        userService.deleteFinishedTimers();
    }
}

