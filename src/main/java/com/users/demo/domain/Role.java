package com.users.demo.domain;

public enum Role {
    USER,
    ADMIN
}
