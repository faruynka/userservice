package com.users.demo.domain;

public enum Status {
    ONLINE,
    AWAY,
    OFFLINE
}
