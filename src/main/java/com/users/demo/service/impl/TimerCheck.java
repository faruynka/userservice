package com.users.demo.service.impl;

import java.util.Timer;
import java.util.TimerTask;

public class TimerCheck extends Timer {
    private boolean isRunning;

    public TimerCheck() {
        super();
        isRunning = false;
    }

    @Override
    public void schedule(TimerTask task, long delay) {
        isRunning = true;
        super.schedule(new TimerTask() {
            @Override
            public void run() {
                task.run();
                isRunning = false;
            }
        }, delay);
    }

    public boolean isRunning() {
        return isRunning;
    }
}
