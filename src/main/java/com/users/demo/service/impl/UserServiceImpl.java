package com.users.demo.service.impl;

import com.users.demo.domain.Status;
import com.users.demo.domain.User;
import com.users.demo.exception.PasswordException;
import com.users.demo.repository.UserRepo;
import com.users.demo.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;

@Service
public class UserServiceImpl implements UserService {
    private Map<Long, TimerCheck> timerMap;
    private final UserRepo userRepo;

    public UserServiceImpl(UserRepo userRepo) {
        this.timerMap = new HashMap<>();
        this.userRepo = userRepo;
    }

    @Override
    @Transactional
    public User add(User user) {
        if (userRepo.findByName(user.getName()).isPresent()) {
            throw new EntityExistsException("User " + user.getName() + " exists");
        }
        return userRepo.save(user);
    }

    @Override
    public User login(String userName, String password) {
        User user = find(userName);
        if (user == null) {
            throw new EntityNotFoundException("User " + userName + " is not found");
        }
        if (!user.getPassword().equals(password)) {
            throw new PasswordException();
        }
        user.setStatus(Status.ONLINE);
        userRepo.save(user);
        scheduleAway(user);
        return user;
    }

    @Override
    @Transactional
    public User find(Long userId) {
        return userRepo.findById(userId).orElseThrow(()
                -> new EntityNotFoundException("User " + userId + " is not found"));
    }

    @Override
    @Transactional
    public User find(String userName) {
        return userRepo.findByName(userName).orElseThrow(()
                -> new EntityNotFoundException("User " + userName + " is not found"));
    }

    @Override
    @Transactional
    public Status changeStatus(Long userId, Status status) {
        User user = find(userId);
        Status currentStatus = user.getStatus();
        System.out.println("STATUS " + status.toString());
        System.out.print("USER " + user.toString());
        user.setStatus(status);
        userRepo.save(user);
        System.out.println("USERAGAIN " + user.toString());
        if (status.equals(Status.ONLINE)) {
            scheduleAway(user);
        }
        return currentStatus;
    }

    @Override
    public void deleteFinishedTimers() {
        timerMap.values().removeIf(TimerCheck::isRunning);
    }

    private void scheduleAway(User user) {
        TimerCheck timer = timerMap.get(user.getId());
        if (timer != null) {
            timer.cancel();
            timerMap.remove(user.getId());
        }
        TimerTask setAway = new TimerTask() {
            @Override
            public void run() {
                changeStatus(user.getId(), Status.AWAY);
            }
        };
        TimerCheck newTimer = new TimerCheck();
        newTimer.schedule(setAway, 60 * 1000 * 5);
        timerMap.put(user.getId(), newTimer);
    }
}
