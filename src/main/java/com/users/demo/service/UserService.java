package com.users.demo.service;

import com.users.demo.domain.Status;
import com.users.demo.domain.User;

public interface UserService {
    User add(User user);

    User find(Long userId);

    User find(String userName);

    Status changeStatus(Long userId, Status status) throws Exception;

    void deleteFinishedTimers();

    User login(String userName, String password);
}
